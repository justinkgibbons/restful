import XCTest

import RestfulTests

var tests = [XCTestCaseEntry]()
tests += RestfulTests.allTests()
XCTMain(tests)