import Foundation

public struct Configuration {

  public init(address: String, method: String, contentType: String) {
    self.address = address; self.method = method; self.contentType = contentType
  }
  
  public let address: String
  public let method: String
  public let contentType: String
}
