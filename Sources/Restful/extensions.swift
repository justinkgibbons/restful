import Foundation

public extension String {

  public func substring(_ startCharacter: Int, _ endCharacter: Int) -> String {
    let start = self.index(self.startIndex, offsetBy: startCharacter)
    let end = self.index(self.startIndex, offsetBy: endCharacter)
    let result = String(self[start...end])
    return result
  }

  public func substringFrom(_ startCharacter: Int) -> String {
    let start = self.index(self.startIndex, offsetBy: startCharacter)
    let result = String(self[start...])
    return result
  }

  public func substringUpTo(_ endCharacter: Int) -> String {
    let end = self.index(self.startIndex, offsetBy: endCharacter)
    let result = String(self[...end])
    return result
  }
}

public extension String {

  var without0x: String {
    return self.substringFrom(2)
  }
}
