import Foundation

public class Restful {

  //MARK: Initializer
  public init() {}

  //MARK: Properties
  public var showResponse = false

  //MARK: Class Methodss
  public static func request(from body: Data?, configuration: Configuration) -> URLRequest? {
    guard let url = URL(string: configuration.address) else { return nil }
    guard let body = body else { return nil }
    var request = URLRequest(url: url)
    request.httpMethod = configuration.method
    request.httpBody = body
    request.setValue(configuration.contentType, forHTTPHeaderField: "content-type")
    return request
  }

  public static func request(from body: Data?, path: String, config: Configuration) -> URLRequest? {
    let endpoint = config.address + path
    guard let url = URL(string: endpoint) else { return nil }
    guard let body = body else { return nil }
    var request = URLRequest(url: url)
    request.httpMethod = config.method
    request.httpBody = body
    return request
  }

  public static func request(from query: String, configuration: Configuration) -> URLRequest? {
    let address = configuration.address + query
    guard let url = URL(string: address) else { return nil }
    var request = URLRequest(url: url)
    request.httpMethod = configuration.method
    request.setValue(configuration.contentType, forHTTPHeaderField: "content-type")
    return request
  }

  //MARK: Methods
  public func send(_ request: URLRequest?, completion: @escaping (Data) -> Void) {
    let session = URLSession.shared
    guard let request = request else { return }

    let task = session.dataTask(with: request) {
      data, response, error in

      if let error = error { print(error) }
      if let response = response, self.showResponse { print(response) }
      if let data = data { completion(data) }
    }

    task.resume()
  }

  public func object<Model: Decodable>(for request: URLRequest?, model: Model.Type, completion: @escaping (Model) -> Void) {
    send(request) { responseData in
      do {
        let result = try JSONDecoder().decode(model, from: responseData)
        completion(result)
      } catch {
        print("Decoding Error: \(error)")
      }
    }
  }

  public func data(for request: URLRequest?, completion: @escaping (Data) -> Void) {
    send(request) { responseData in
      completion(responseData)
    }
  }
}
